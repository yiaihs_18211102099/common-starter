package com.yishuifengxiao.common.core;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.yishuifengxiao.common.exception.ExceptionProperties;
import com.yishuifengxiao.common.support.ErrorUtil;
import com.yishuifengxiao.common.support.SpringContext;

import lombok.extern.slf4j.Slf4j;

/**
 * 核心组件自动配置
 * 
 * @author yishui
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(ExceptionProperties.class)
public class CommonAutoConfiguration {

	/**
	 * 注入一个spring 上下文工具类
	 * 
	 * @param applicationContext spring上下文
	 * @return spring 上下文工具类
	 */
	@Bean
	public SpringContext springContext(ApplicationContext applicationContext) {
		SpringContext springContext = new SpringContext();
		springContext.setApplicationContext(applicationContext);
		return springContext;
	}

	/**
	 * 异常信息提取工具
	 * 
	 * @param exceptionProperties 异常信息配置规则
	 * @return 异常信息提取工具
	 */
	@Bean
	public ErrorUtil errorUtil(ExceptionProperties exceptionProperties) {
		ErrorUtil errorUtil = new ErrorUtil();
		errorUtil.init(exceptionProperties);
		return errorUtil;
	}

	/**
	 * <p>
	 * 注入一个guava异步消息总线
	 * </p>
	 * 
	 * 使用该异步消息总线的示例如下：
	 * 
	 * <pre>
	 * &#64;Component
	 * public class DemoEventBus {
	 * 	&#64;Resource
	 * 	private EventBus asyncEventBus;
	 * 
	 * 	&#64;PostConstruct
	 * 	public void init() {
	 * 		asyncEventBus.register(this);
	 * 	}
	 * }
	 * 
	 * </pre>
	 * 
	 * 
	 * 发送消息
	 * 
	 * <pre>
	 * asyncEventBus.post("需要发送的数据");
	 * </pre>
	 * 
	 * 接收消息
	 * 
	 * <pre>
	 * &#64;Subscribe
	 * public void recieve(Object data) {
	 * 	// 注意guava是根据入参的数据类型进行接收的
	 * 	// 发送的数据可以被多个接收者同时接收
	 * }
	 * </pre>
	 * 
	 * @return guava异步消息总线
	 */
	@Bean("asyncEventBus")
	@ConditionalOnMissingBean(name = { "asyncEventBus" })
	public EventBus asyncEventBus() {
		BlockingQueue<Runnable> queue = new LinkedBlockingDeque<>(100);
		ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 5, 5, TimeUnit.SECONDS, queue,
				new ThreadPoolExecutor.CallerRunsPolicy());
		return new AsyncEventBus(executor);
	}

	/**
	 * 配置检查
	 */
	@PostConstruct
	public void checkConfig() {

		log.trace("【易水组件】: 开启 <全局通用支持> 相关的配置");
	}

}
